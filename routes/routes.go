package routes

import (
	"net/http"
	"strings"
	"../gorilla/mux"
	"../controllers"
)

/*
 * create all the routes
 */
func SetUp() {
	r := mux.NewRouter()
	r.HandleFunc("/todos", todosHandler)
	r.HandleFunc("/todos/{id:[0-9]+}", todoHandler)
	r.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(http.Dir("./static"))))
	http.Handle("/", r)
}

/*
 * figure out if we need to handle a get all todos request or
 * handle a create todo request
 */
func todosHandler(w http.ResponseWriter, req *http.Request) {
	if req.Method == `GET` {
		todos_controllers.Index(w, req)
	} else if req.Method == `POST` {
		req.ParseForm()
		todos_controllers.Create(w, req)
	}
}

/*
 * determine if we need to handle a PUT, DELETE or GET request to 
 * todos
 */
func todoHandler(w http.ResponseWriter, req *http.Request) {
	formMethod := strings.ToUpper(req.FormValue("_method"))
	reqMethod := strings.ToUpper(req.Method)

	if (reqMethod == `POST` && formMethod == `PUT`) || reqMethod == `PUT` {
		todos_controllers.Update(w, req)
	} else if (reqMethod == `POST` && formMethod == `DELETE`) || reqMethod == `DELETE` {
		todos_controllers.Delete(w, req)
	} else if req.Method == `GET` {
		todos_controllers.Show(w, req)
	}
}
