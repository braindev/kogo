package main

import (
	"net/http"
	"log"
	"./routes"
	"./dbpool"
)

func main() {
	dbpool.CreateDbPool()
	routes.SetUp()
	err := http.ListenAndServe(":5050", Log(http.DefaultServeMux))
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func Log(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s %s %s", r.RemoteAddr, r.Method, r.URL)
		handler.ServeHTTP(w, r)
	})
}


