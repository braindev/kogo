package todos_controllers

import (
	"net/http"
	"io"
	"encoding/json"
	"fmt"
	"strconv"

	"../models/todo"
	"../gorilla/mux"
)

func Index(w http.ResponseWriter, req *http.Request) {
	todos, err := todo.All()
	if err != nil {
		fmt.Println(err)
		io.WriteString(w, `{}`)
	} else {
		b, err := json.Marshal(todos)
		if err != nil {
			fmt.Println(err)
			io.WriteString(w, `{}`)
		} else {
			io.WriteString(w, string(b))
		}
	}
}

func Create(w http.ResponseWriter, req *http.Request) {
	req.ParseForm()
	jsonStr := req.FormValue("json")
	id, err := todo.CreateFromJSON(jsonStr)
	if (err != nil) {
		io.WriteString(w, `{"error": true}`)
	} else {
		io.WriteString(w, "{\"id\": "+strconv.Itoa(id)+"}")
	}
}

func Update(w http.ResponseWriter, req *http.Request) {
	var response []byte
	req.ParseForm()
	urlVars := mux.Vars(req)
	var id int
	id, err := strconv.Atoi(urlVars["id"]); 
	if err != nil {
		response, _ = json.Marshal(map[string]interface{}{"error": true})
	} else {
		jsonStr := req.FormValue("json")
		if todo, err := todo.UpdateFromJSON(jsonStr, id); err != nil {
			response, _ = json.Marshal(map[string]interface{}{"error": true})
		} else {
			response, _ = json.Marshal(todo)
		}
	}
	io.WriteString(w, string(response))
}

func Show(w http.ResponseWriter, req *http.Request) {
}

func Delete(w http.ResponseWriter, req *http.Request) {
	var response []byte
	req.ParseForm()
	urlVars := mux.Vars(req)
	var id int
	id, err := strconv.Atoi(urlVars["id"]); 
	if err != nil {
		response, _ = json.Marshal(map[string]interface{}{"error": true})
	} else {
		if err := todo.Delete(id); err != nil {
			response, _ = json.Marshal(map[string]interface{}{"error": true})
		} else {
			response, _ = json.Marshal(map[string]interface{}{"deleted": true})
		}
	}
	io.WriteString(w, string(response))
}
