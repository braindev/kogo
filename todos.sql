CREATE TABLE "public"."todos" (
	"id" serial,
	"name" text NOT NULL,
	"priority" int2 NOT NULL,
	CONSTRAINT "todos_pkey" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE
)
WITH (OIDS=FALSE);
