package todo

import (
	"encoding/json"
	"database/sql"
	"fmt"
	_ "../../pq"
	"./../../dbpool"
)

type Todo struct {
	Id       int    `json:"id"`
	Name     string `json:"name"`
	Priority int    `json:"priority"`
}

/*
 * return all todos
 */
func All() ([]Todo, error) {
	var todos []Todo
	db := dbpool.DbPool.Acquire()
	defer dbpool.DbPool.Release(db)
	if rows, err := db.(*sql.DB).Query("select id, name, priority from todos"); err != nil {
		return nil, err
	} else {
		for rows.Next() {
			todo := new(Todo)
			rows.Scan(&todo.Id, &todo.Name, &todo.Priority)
			todos = append(todos, *todo)
		}
	}
	return todos, nil
}

/*
 * create a todo from a json string
 */
func CreateFromJSON(jsonStr string) (int, error) {
	var todo Todo
	err := json.Unmarshal([]byte(jsonStr), &todo)
	if err == nil {
		db := dbpool.DbPool.Acquire()
		defer dbpool.DbPool.Release(db)
		row := db.(*sql.DB).QueryRow("insert into todos (name, priority) values ($1, $2) returning id", todo.Name, todo.Priority)
		row.Scan(&todo.Id)
		fmt.Println(todo.Id)
	} else {
		fmt.Println(err)
	}
	return todo.Id, err
}

/*
 * update a todo from a json string
 */
func UpdateFromJSON(jsonStr string, id int) (Todo, error) {
	var todo Todo
	if err := json.Unmarshal([]byte(jsonStr), &todo); err == nil {
		db := dbpool.DbPool.Acquire().(*sql.DB)
		defer dbpool.DbPool.Release(db)
		todo.Id = id
		if _, err := db.Exec("update todos set name = $1, priority = $2 where id = $3", todo.Name, todo.Priority, id); err != nil {
			return Todo{}, err
		}
	} else {
		return Todo{}, err
	}
	return todo, nil
}

/*
 * delete todo by id
 */
func Delete(id int) (error) {
	db := dbpool.DbPool.Acquire().(*sql.DB)
	defer dbpool.DbPool.Release(db)
	if _, err := db.Exec("delete from todos where id = $1", id); err != nil {
		return err
	}
	return nil
}
