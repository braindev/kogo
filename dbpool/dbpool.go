package dbpool

import (
	"database/sql"
	_ "./../pq"
	"./../gopool"
)

var DbPool *gopool.Pool

func CreateDbPool() {
	create := func() (interface{}) {
		db, _ := sql.Open("postgres", "user=bjohnson dbname=bjohnson sslmode=disable")
		return db
	}
	destroy := func(resource interface{}) {
	}
	DbPool = gopool.Initialize(5, create, destroy)
}
