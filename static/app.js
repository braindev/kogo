/*global ko:true, _:true*/
(function($){

  var deleteTodo = function (id, callback) {
    $.ajax({
      url: '/todos/' + id,
      dataType: 'json',
      success: function() {
        callback();
      },
      type: 'DELETE'
    });
  };

  var persistTodo = function (todoViewModel, callback) {
    var data = {
      json: JSON.stringify({
        name: todoViewModel.name(),
        priority: todoViewModel.priority()
      })
    };
    $.ajax({
      url: '/todos',
      dataType: 'json',
      data: data,
      success: function (data) {
        todoViewModel.id(data.id);
        todoViewModel._persisted(true);
        if (callback) {
          callback();
        }
      },
      type: 'POST'
    });
  };

  var TodosViewModel = function () {
    var self = this;

    self.todos = ko.observableArray();
    self.orderedTodos = ko.computed(function () {
      // TODO return list of todos ordered by priority
      return self.todos;
    });
    self.newTodoName = ko.observable('');

    self.addTodo = function () {
      var newTodo = new TodoViewModel();
      newTodo.name(self.newTodoName());
      newTodo.priority(10);
      newTodo._persisted(false);
      self.todos.push(newTodo);
      persistTodo(newTodo);
      self.newTodoName('');
    };

    self.deleteTodo = function (el) {
      deleteTodo(el.id(), function () {
          self.todos.remove(el);
      });
    };
  };

  var TodoViewModel = function () {
    var self = this;
    self.id = ko.observable();
    self.name = ko.observable();
    self.priority = ko.observable(0);
    self.complete = ko.observable(false);
    self._persisted = ko.observable(false);
  };

  var loadTodosFromServer = function (todosViewModel) {
    $.ajax({
      url: '/todos',
      dataType: 'json',
      error: function (jqXHR, textStatus, errorThrown) {
      },
      success: function (data, textStatus, jqXHR) {
        var todoViewModels = [];
        _(data).each(function (td) {
          var todoViewModel = new TodoViewModel();
          todoViewModel.name(td.name);
          todoViewModel.id(td.id);
          todoViewModel.priority(td.priority);
          todoViewModel._persisted(true);
          todoViewModels.push(todoViewModel);
        });
        todosViewModel.todos(todoViewModels);
      }
    });
  };
  $(function(){
    var todosViewModel = new TodosViewModel();
    ko.applyBindings(todosViewModel);
    loadTodosFromServer(todosViewModel);
  });
}(jQuery));
