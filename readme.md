# KOGO = Knockout + Golang + TODOs

A quick little project to learn some golang in the context of providing a REST web service.

- load `todos.sql` into a postgres database
- edit `dbpool/dbpool.go` to match your database settings
- run `go run main.go` from the project root directory
- load [http://localhost:5050/](http://localhost:5050/) in a web browser
